package sprise.projecttabungan.util;

import com.google.gson.Gson;

import sprise.projecttabungan.model.WishModel;

/**
 * Created by Andrea's on 5/17/2016.
 */
public class GsonJson {

    public GsonJson() {
    }

    public String convertGsonJson(Object obj){
        return new Gson().toJson(obj);
    }

    public WishModel convertJsonGson(String json){
        return new Gson().fromJson(json, WishModel.class);
    }
}
