package sprise.projecttabungan.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import sprise.projecttabungan.model.WishModel;

/**
 * Created by Andrea's on 5/5/2016.
 */
public class SaveSharedPreferences {

    static final String PREF_USER_CURRENT_MONEY = "current_money";
    static final String PREF_WISHLIST_MODEL= "wish_model";
    static final String PREF_FROM_WISHLIST_KEY = "is_wishview";
    static final String PREF_WISH_SAVED_MONEY_KEY = "saved_money";
    static final String PREF_INCOME_TOTAL_KEY = "income_total";
    static final String PREF_OUTCOME_TOTAL_KEY = "outcome_total";

    static SharedPreferences getSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setCurrentMoney(Context context, int money){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(PREF_USER_CURRENT_MONEY, money);
        editor.commit();
    }

    public static int getCurrentMoney(Context context){
        return getSharedPreferences(context).getInt(PREF_USER_CURRENT_MONEY, 0);
    }

    public static void setWishModel(Context context, WishModel wishModel){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        String wishObjJson = new GsonJson().convertGsonJson(wishModel);
        editor.putString(PREF_WISHLIST_MODEL, wishObjJson);
        editor.commit();
    }

    public static WishModel getWishModel(Context context){
        return new GsonJson().convertJsonGson(
                        getSharedPreferences(context).getString(PREF_WISHLIST_MODEL, null)
                );
    }

    private static String changeToGson(WishModel wishModel){
        return new Gson().toJson(wishModel);
    }

    public static void setSavedWishCash(Context context, int savedCash){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(PREF_WISH_SAVED_MONEY_KEY, savedCash);
        editor.commit();
    }

    public static int getSavedWishCash(Context context){
        return getSharedPreferences(context).getInt(PREF_WISH_SAVED_MONEY_KEY, 0);
    }

    public static void setFromWish(Context context, boolean isFromWish){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_FROM_WISHLIST_KEY, isFromWish);
        editor.commit();
    }

    public static boolean getFromWish(Context context){
        return getSharedPreferences(context).getBoolean(PREF_FROM_WISHLIST_KEY, false);
    }

    public static void setTotalIncome(Context context, int income){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(PREF_INCOME_TOTAL_KEY, income);
        editor.commit();
    }

    public static int getTotalIncome(Context context){
        return getSharedPreferences(context).getInt(PREF_INCOME_TOTAL_KEY, 0);
    }

    public static void setTotalOutcome(Context context, int outcome){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(PREF_OUTCOME_TOTAL_KEY, outcome);
        editor.commit();
    }

    public static int getTotalOutcome(Context context){
        return getSharedPreferences(context).getInt(PREF_OUTCOME_TOTAL_KEY, 0);
    }
}
