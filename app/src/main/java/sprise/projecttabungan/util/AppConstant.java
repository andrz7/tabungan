package sprise.projecttabungan.util;

/**
 * Created by Andrea's on 5/7/2016.
 */
public class AppConstant {
    public static String EXTRA_TYPE_ADD_MONEY = "add_money_user";
    public static String CONSTANT_TYPE_ADD_USER_MONEY = "first_money";
    public static String CONSTANT_TYPE_ADD_INCOME = "in_come";
    public static String CONSTANT_TYPE_ADD_OUTCOME = "out_come";
    public static String CONSTANT_TYPE_WISH_ADD_MONEY = "wish_view_add_money";
    public static final String CONSTANT_TYPE_INCOME = "Income";
    public static final String CONSTANT_TYPE_OUTCOME = "Outcome";

}
