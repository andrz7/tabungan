package sprise.projecttabungan.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Andrea's on 5/22/2016.
 */
public class DateHelper {

    public DateHelper() {
    }

    public String getCurrentDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(new Date());
        return date;
    }

    public String sqliteDateFormatter(String dateChanged) throws ParseException {
        String finalDate = null;
        Date date = null;
        SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        date = (Date)dateFormat.parse(dateChanged);
        finalDate = newFormat.format(date);
        return finalDate;
    }

    public static String getMonth(String date) throws ParseException{
        Date d = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        String monthName = new SimpleDateFormat("MMMM yyyy").format(cal.getTime());
        return monthName;
    }

    public static String getMonthNameBasedOnInt(int monthInNumber){
        String monthName = null;
        switch (monthInNumber){
            case 1:
                monthName = "January";
                break;
            case 2:
                monthName = "February";
                break;
            case 3:
                monthName = "March";
                break;
            case 4:
                monthName = "April";
                break;
            case 5:
                monthName = "May";
                break;
            case 6:
                monthName = "June";
                break;
            case 7:
                monthName = "July";
                break;
            case 8:
                monthName = "August";
                break;
            case 9:
                monthName = "September";
                break;
            case 10:
                monthName = "October";
                break;
            case 11:
                monthName = "November";
                break;
            case 12:
                monthName = "December";
                break;
            default:
                monthName = "Unidentified";
                break;
        }

        return monthName;
    }

}
