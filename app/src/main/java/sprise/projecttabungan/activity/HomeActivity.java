package sprise.projecttabungan.activity;

import android.content.res.Configuration;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.TextView;
import android.widget.Toast;


import java.lang.reflect.Field;

import sprise.projecttabungan.R;
import sprise.projecttabungan.databasesqlite.TransactionDAO;
import sprise.projecttabungan.fragment.AddWishlistFragment;
import sprise.projecttabungan.fragment.HomeFragment;
import sprise.projecttabungan.fragment.WishViewFragment;
import sprise.projecttabungan.util.DateHelper;
import sprise.projecttabungan.util.SaveSharedPreferences;

public class HomeActivity extends BaseActivity {

    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;
    private TransactionDAO transactionDAO;
    private DrawerLayout mDrawer;

    private TextView incomeView, totalMoneyView, outcomeView;
    public static Toolbar toolbar;

    private String willShownUserCurrentMoney, willShownTotalIncome, willShownTotalOutcome;
    private long currentUserIncome, totalIncome, totalOutcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_view_home, new HomeFragment());
            fragmentTransaction.commit();
        initUI();
        setToolbar();
        setupDrawerContent(navigationView);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public void initUI(){
        navigationView = (NavigationView) findViewById(R.id.navigation);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer);
        transactionDAO = new TransactionDAO();
    }

    public void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Home");
        }
    }

    private ActionBarDrawerToggle setUpDrawerToggle(){
        return new ActionBarDrawerToggle(
                this,
                mDrawer,
                toolbar,
                R.string.open_navigation,
                R.string.close_navigation
        );
    }

    @Override
    protected void onResume() {
        super.onResume();

        totalIncome = transactionDAO.getTotalIncome(this, new DateHelper().getCurrentDate());
        totalOutcome = transactionDAO.getTotalOutcome(this, new DateHelper().getCurrentDate());

        if (totalOutcome > totalIncome)
            currentUserIncome = 0;
        else
            currentUserIncome = totalIncome - totalOutcome;

        willShownTotalOutcome = String.format("Outcome  : IDR %,d", totalOutcome);
        willShownTotalIncome = String.format("Income    : IDR %,d", totalIncome);
        willShownUserCurrentMoney = String.format("%,d", currentUserIncome);

        totalMoneyView.setText("Balance : IDR " + willShownUserCurrentMoney);
        outcomeView.setText(willShownTotalOutcome);
        incomeView.setText(willShownTotalIncome);
    }

    private void setupDrawerContent(NavigationView navigationView){
        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header);

        totalMoneyView = (TextView) headerLayout.findViewById(R.id.total_money_txt_view);
        outcomeView = (TextView) headerLayout.findViewById(R.id.total_outcome_txt_view);
        incomeView = (TextView) headerLayout.findViewById(R.id.total_income_txt_view);

        drawerToggle = setUpDrawerToggle();
        mDrawer.addDrawerListener(drawerToggle);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                selectDrawerItem(item);
                return true;
            }
        });
    }

    public void selectDrawerItem(MenuItem menuItem){
        Fragment fragment = null;
        Class fragmentClass = null;
        switch (menuItem.getItemId()){
            case R.id.navigation_home:
                fragmentClass = HomeFragment.class;
                break;
            case R.id.navigation_debt:
                fragmentClass = null;
                break;
            case R.id.navigation_wish:
                fragmentClass = SaveSharedPreferences.getWishModel(this) == null ?
                        AddWishlistFragment.class : WishViewFragment.class;
                break;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
        if (fragment != null){
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_view_home, fragment).commit();
            menuItem.setChecked(true);
            setTitle(menuItem.getTitle());
            mDrawer.closeDrawers();
        }else{
            Toast.makeText(HomeActivity.this, "On Progress...", Toast.LENGTH_SHORT).show();
        }
    }

    public void setToolbarTitle(String title, int elevation){
        if (getSupportActionBar() != null){
            toolbar.setElevation(elevation);
            getSupportActionBar().setTitle(title);
        }
    }

    private void showOverflowMenu(FragmentActivity activity) {
        try {
            ViewConfiguration config = ViewConfiguration.get(activity);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
