package sprise.projecttabungan.activity;

import android.content.Intent;
import android.os.Build;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import sprise.projecttabungan.R;
import sprise.projecttabungan.fragment.AddMoneyFragment;
import sprise.projecttabungan.fragment.AddMoneyIncomeFragment;
import sprise.projecttabungan.fragment.AddMoneyOutcomeFragment;
import sprise.projecttabungan.util.AppConstant;

public class AddMoney extends BaseActivity {

    private String TAG = AddMoney.class.getSimpleName();
    private Toolbar toolbar;
    private String addType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);
        initToolbar();
        getAddMoneyType();
        setLayoutFragment(savedInstanceState);
    }

    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.activity_addmoney_title);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.toolbar.setElevation(20);
        }
    }

    private void setLayoutFragment(Bundle savedInstanceState){
        if (savedInstanceState == null){
            if (addType.equalsIgnoreCase(AppConstant.CONSTANT_TYPE_ADD_USER_MONEY)){
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.add_money_frame, new AddMoneyFragment());
                fragmentTransaction.commit();
            }else if (addType.equalsIgnoreCase(AppConstant.CONSTANT_TYPE_ADD_INCOME)){
                Log.v(TAG, "income");
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.add_money_frame, new AddMoneyIncomeFragment());
                fragmentTransaction.commit();
            }else if (addType.equalsIgnoreCase(AppConstant.CONSTANT_TYPE_ADD_OUTCOME)){
                Log.v(TAG, "outcome");
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.add_money_frame, new AddMoneyOutcomeFragment());
                fragmentTransaction.commit();
            }else if (addType.equalsIgnoreCase(AppConstant.CONSTANT_TYPE_WISH_ADD_MONEY)){
                Log.v(TAG, "wish");
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.add_money_frame, new AddMoneyFragment());
                fragmentTransaction.commit();
            }
        }
    }

    private void getAddMoneyType(){
        Bundle bundle = getIntent().getExtras();
        addType = bundle.getString(AppConstant.EXTRA_TYPE_ADD_MONEY);
        Log.v(TAG, addType);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (addType.equalsIgnoreCase(AppConstant.CONSTANT_TYPE_WISH_ADD_MONEY))
            finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
