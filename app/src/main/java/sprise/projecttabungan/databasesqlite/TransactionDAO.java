package sprise.projecttabungan.databasesqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import sprise.projecttabungan.model.TransactionHIstoryMonthly;
import sprise.projecttabungan.model.TransactionHistoryModel;
import sprise.projecttabungan.model.YearTransaction;
import sprise.projecttabungan.util.AppConstant;

/**
 * Created by Andrea's on 5/22/2016.
 */
public class TransactionDAO {

    public void addNewTransaction(Context context, String category, String type, long amount
            , String timestamp){
        SQLiteDatabase db = DataHelper.getsInstance(context).getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DataHelper.FIELD_CATEGORY, category);
        cv.put(DataHelper.FIELD_TYPE, type);
        cv.put(DataHelper.FIELD_AMOUNT, amount);
        cv.put(DataHelper.FIELD_DATE, timestamp);
        db.insertWithOnConflict(DataHelper.TABLE_TRANSACTION_HISTORY, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
    }

    public List<TransactionHistoryModel> getHistoryDaily(Context ctx, String timestamp){
        Log.v("orer", timestamp);
        List<TransactionHistoryModel> historys = new ArrayList<>();
        String category, type, date;
        long amount;

        SQLiteDatabase db = DataHelper.getsInstance(ctx).getReadableDatabase();
        String selection = DataHelper.FIELD_DATE +"=?";
        String[] selectionArgs = {timestamp};
        Cursor cursor = db.query(DataHelper.TABLE_TRANSACTION_HISTORY,null,selection,selectionArgs
                ,null,null,null);

        while(cursor.moveToNext()){
            category = cursor.getString(cursor.getColumnIndex(DataHelper.FIELD_CATEGORY));
            type = cursor.getString(cursor.getColumnIndex(DataHelper.FIELD_TYPE));
            amount = cursor.getLong(cursor.getColumnIndex(DataHelper.FIELD_AMOUNT));
            date = cursor.getString(cursor.getColumnIndex(DataHelper.FIELD_DATE));
            historys.add(new TransactionHistoryModel(category, type, amount, date));
        }
        return historys;
    }

    public long getTotal(Context context, String date){
        long totalMoney = 0;
        long tempMoney;
        String tempType;

        SQLiteDatabase db = DataHelper.getsInstance(context).getReadableDatabase();
        String selection = DataHelper.FIELD_DATE + "=?";
        String[] selectionArgs = {date};
        Cursor cursor = db.query(DataHelper.TABLE_TRANSACTION_HISTORY, null, selection, selectionArgs,
                null, null, null);

        if (cursor.getCount() != 0){
            while (cursor.moveToNext()){
                tempType = cursor.getString(cursor.getColumnIndex(DataHelper.FIELD_TYPE));
                tempMoney = cursor.getLong(cursor.getColumnIndex(DataHelper.FIELD_AMOUNT));
                if (tempType.equalsIgnoreCase(AppConstant.CONSTANT_TYPE_INCOME)){
                    totalMoney += tempMoney;
                }else if (tempType.equalsIgnoreCase(AppConstant.CONSTANT_TYPE_OUTCOME)){
                    if (tempMoney >= totalMoney)
                        totalMoney = 0;
                    else
                        totalMoney -= tempMoney;
                }
            }
        }

        return totalMoney;
    }

    public long getTotalVersionTwo(Context context, String date){
        long income = getTotalIncome(context, date);
        long outcome = getTotalOutcome(context, date);
        if(outcome > income)
            return 0;
        return income - outcome;
    }

    public long getTotalIncome(Context context, String date){
        long totalIncome = 0;

        SQLiteDatabase db = DataHelper.getsInstance(context).getReadableDatabase();
        String selection = DataHelper.FIELD_DATE + "=? AND " + DataHelper.FIELD_TYPE + "=?";
        String[] selectionArgs = {date, AppConstant.CONSTANT_TYPE_INCOME};
        Cursor cursor = db.query(DataHelper.TABLE_TRANSACTION_HISTORY, null, selection, selectionArgs,
                null, null, null);

        while (cursor.moveToNext()){
            totalIncome += cursor.getLong(cursor.getColumnIndex(DataHelper.FIELD_AMOUNT));
        }

        return totalIncome;
    }

    public long getTotalOutcome(Context context, String date){
        long totalOutcome = 0;

        SQLiteDatabase db = DataHelper.getsInstance(context).getReadableDatabase();
        String selection = DataHelper.FIELD_DATE + "=? AND " + DataHelper.FIELD_TYPE + "=?";
        String[] selectionArgs = {date, AppConstant.CONSTANT_TYPE_OUTCOME};
        Cursor cursor = db.query(DataHelper.TABLE_TRANSACTION_HISTORY, null, selection, selectionArgs,
                null, null, null);

        while (cursor.moveToNext()){
            totalOutcome += cursor.getLong(cursor.getColumnIndex(DataHelper.FIELD_AMOUNT));
        }

        return totalOutcome;
    }

    public List<TransactionHIstoryMonthly> getMonthlyList(Context context, String date){
        List<TransactionHIstoryMonthly> data = new ArrayList<>();
        SQLiteDatabase db = DataHelper.getsInstance(context).getReadableDatabase();
        String[] dates = date.split("-");
        Log.v("dates", dates[1] + "- " + dates[2] + " - " + dates[0]);
        String query = "select \n" +
                "    strftime('%W', transaction_date) WeekNumber,\n" +
                "    max(date(transaction_date, 'weekday 0', '-7 day')) WeekStart,\n" +
                "    max(date(transaction_date, 'weekday 0', '-1 day')) WeekEnd,\n" +
                "    count(*) as GroupedValues, " +
                "sum(case when type = 'Income' then amount end) as income,\n" +
                "sum(case when type = 'Outcome' then amount end) as outcome,\n" +
                "sum(case when type = 'Income' then amount else -amount end) Total\n" +
                "from MsHistory\n" +
                "where strftime('%Y', transaction_date) = '"+ dates[0]
                +"' and strftime('%m', transaction_date) = '"+ dates[1] +"' group by WeekNumber;";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()){
            String time = cursor.getString(0);
            String weekstart = cursor.getString(1);
            String weekend = cursor.getString(2);
            int count = cursor.getInt(3);
            int income = cursor.getInt(4);
            int outcome = cursor.getInt(5);
            int amount = cursor.getInt(6);
            Log.v("tes", String.format("%s %s %s %d %d %d", time, weekstart, weekend, income, outcome, amount));
            data.add(new TransactionHIstoryMonthly((long) amount, (long) income, (long) outcome, weekstart, weekend));
        }
        return data;
    }

    public List<YearTransaction> getYearlyList(Context context, String date){
        List<YearTransaction> data = new ArrayList<>();
        SQLiteDatabase db = DataHelper.getsInstance(context).getReadableDatabase();
        String[] dates = date.split("-");
        Log.v("dates", dates[1] + "- " + dates[2] + " - " + dates[0]);
        String query = "select sum(case when type = 'Income' then amount end) as total_income, " +
                "sum(case when type = 'Outcome' then amount end) as total_outcome, " +
                "strftime('%m', transaction_date) month_number from MsHistory " +
                "where strftime('%Y') = '" + dates[0] + "' group by month_number";
        Cursor cursor = db.rawQuery(query, null);
        while(cursor.moveToNext()){
            int totalIncome = cursor.getInt(0);
            int totalOutcome = cursor.getInt(1);
            int totalAmount = totalIncome - totalOutcome;
            String time = cursor.getString(2);
            Log.v("testing", String.format("%d %d %s", totalIncome, totalOutcome, time));
            data.add(new YearTransaction((long) totalIncome, (long) totalOutcome, (long) totalAmount, time));
        }

        return data;
    }

}
