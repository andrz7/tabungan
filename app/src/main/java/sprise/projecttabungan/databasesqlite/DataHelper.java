package sprise.projecttabungan.databasesqlite;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Andrea's on 5/22/2016.
 */
public class DataHelper extends SQLiteOpenHelper{

    private static DataHelper sInstance;

    private static final String DB_NAME = "transactions";
    static final int DB_VERSION = 1;

    public static final String TABLE_TRANSACTION_HISTORY = "MsHistory";
    public static final String FIELD_TOTAL_MONTH = "total_month";
    public static final String FIELD_DATE = "transaction_date";
    public static final String FIELD_CATEGORY = "category";
    public static final String FIELD_AMOUNT = "amount";
    public static final String FIELD_TYPE = "type";
    public static final String FIELD_ID = "_id";

    public DataHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static synchronized DataHelper getsInstance(Context context){
        if (sInstance == null)
            sInstance = new DataHelper(context.getApplicationContext());
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String create = "CREATE TABLE IF NOT EXISTS '"+TABLE_TRANSACTION_HISTORY+"'(\n"+
                "'"+FIELD_ID+"' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+"'"+FIELD_CATEGORY+"' TEXT, "+
                "'" + FIELD_TYPE +"' TEXT," + "'" + FIELD_AMOUNT + "' LONG, '" + FIELD_DATE
                + "' DATETIME DEFAULT CURRENT_TIMESTAMP);";

        db.execSQL(create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
