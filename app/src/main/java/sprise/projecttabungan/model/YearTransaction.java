package sprise.projecttabungan.model;

/**
 * Created by Andrea's on 9/6/2016.
 */
public class YearTransaction {
    private long totalIncome, totalOutcome, totalAmount;
    private String time;

    public YearTransaction(long totalIncome, long totalOutcome, long totalAmount, String time) {
        this.totalIncome = totalIncome;
        this.totalOutcome = totalOutcome;
        this.totalAmount = totalAmount;
        this.time = time;
    }

    public long getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(long totalIncome) {
        this.totalIncome = totalIncome;
    }

    public long getTotalOutcome() {
        return totalOutcome;
    }

    public void setTotalOutcome(long totalOutcome) {
        this.totalOutcome = totalOutcome;
    }

    public long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
