package sprise.projecttabungan.model;

/**
 * Created by Andrea's on 6/4/2016.
 */
public class TransactionHistoryModel {

    private String category;
    private String type;
    private String date;
    private long amount;

    public TransactionHistoryModel(String category, String type, long amount, String date) {
        this.type = type;
        this.category = category;
        this.amount = amount;
        this.date = date;
    }

    public String getType(){
        return type;
    }

    public String getCategory() {
        return category;
    }

    public long getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }
}
