package sprise.projecttabungan.model;

/**
 * Created by Andrea's on 6/10/2016.
 */
public class TransactionHIstoryMonthly {
    private String weekStart, weekEnd;
    private long amountMonthly, amountIncome, amountOutcome;

    public TransactionHIstoryMonthly(long amountMonthly, long amountIncome
            , long amountOutcome, String weekStart, String weekEnd) {
        this.amountMonthly = amountMonthly;
        this.weekStart = weekStart;
        this.weekEnd = weekEnd;
        this.amountIncome = amountIncome;
        this.amountOutcome = amountOutcome;
    }

    public long getAmountIncome() {
        return amountIncome;
    }

    public long getAmountOutcome() {
        return amountOutcome;
    }

    public String getWeekEnd() {
        return weekEnd;
    }

    public long getAmountMonthly() {
        return amountMonthly;
    }

    public void setAmountMonthly(long amountMonthly) {
        this.amountMonthly = amountMonthly;
    }

    public String getWeekStart() {
        return weekStart;
    }

    public void setWeekStart(String weekStart) {
        this.weekStart = weekStart;
    }
}
