package sprise.projecttabungan.model;

/**
 * Created by Andrea's on 5/17/2016.
 */
public class WishModel {

    private String goalStartDate;
    private String goalImageUrl;
    private String goalEndDate;
    private String goalName;
    private int goalPrice;

    public String getGoalImageUrl() {
        return goalImageUrl;
    }

    public String getGoalName() {
        return goalName;
    }

    public String getGoalStartDate() {
        return goalStartDate;
    }

    public String getGoalEndDate() {
        return goalEndDate;
    }

    public int getGoalPrice() {
        return goalPrice;
    }

    public WishModel(String goalImageUrl, String goalName, String goalStartDate
            , String goalEndDate, int goalPrice) {
        this.goalImageUrl = goalImageUrl;
        this.goalName = goalName;
        this.goalStartDate = goalStartDate;
        this.goalEndDate = goalEndDate;
        this.goalPrice = goalPrice;
    }
}
