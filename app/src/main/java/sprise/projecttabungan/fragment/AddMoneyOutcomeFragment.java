package sprise.projecttabungan.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sprise.projecttabungan.R;
import sprise.projecttabungan.activity.HomeActivity;
import sprise.projecttabungan.adapter.CategoryAdapter;
import sprise.projecttabungan.adapter.CustomItemClickListener;
import sprise.projecttabungan.databasesqlite.TransactionDAO;
import sprise.projecttabungan.util.AppConstant;
import sprise.projecttabungan.util.DateHelper;
import sprise.projecttabungan.util.SaveSharedPreferences;

/**
 * Created by Andrea's on 5/8/2016.
 */
public class AddMoneyOutcomeFragment extends Fragment {

    private static final String TAG = AddMoneyOutcomeFragment.class.getSimpleName();
    private static final int SPAN_COUNT = 3
            ;
    private RecyclerView recyclerViewCategory;
    private EditText editTextOutcome;
    private CategoryAdapter adapter;
    private TransactionDAO transactionDAO;
    private List<String> categoryList;
    private String amountMoneyInStr, category;

    private String timestamp;
    private int amountMoneyInInte;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categoryList = new ArrayList<>();
        transactionDAO = new TransactionDAO();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_addmoney_outcome, container, false);
        recyclerViewCategory = (RecyclerView) view.findViewById(R.id.recycler_category);
        editTextOutcome = (EditText) view.findViewById(R.id.edit_outcome);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRecyclerViewCategory();
    }

    private void setRecyclerViewCategory(){
        categoryList = Arrays.asList(getResources()
                .getStringArray(R.array.array_for_category_outcome));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(
                getActivity(),
                SPAN_COUNT,
                LinearLayoutManager.VERTICAL,
                false
        );

        recyclerViewCategory.setLayoutManager(gridLayoutManager);
        adapter = new CategoryAdapter(getActivity(), categoryList, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                eventCategoryCardClicked(position);
            }
        });
        recyclerViewCategory.setAdapter(adapter);
    }

    private void eventCategoryCardClicked(int position){
        amountMoneyInStr = editTextOutcome.getText().toString();
        amountMoneyInInte = Integer.parseInt(amountMoneyInStr);
        category = categoryList.get(position);
        timestamp = new DateHelper().getCurrentDate();
        Log.v(TAG, categoryList.get(position) + amountMoneyInStr);
        countTotal(amountMoneyInInte);

        /*Save to db Sqlite*/
        transactionDAO.addNewTransaction(getActivity()
                , category
                , AppConstant.CONSTANT_TYPE_OUTCOME
                , Integer.parseInt(amountMoneyInStr)
                , timestamp);
        startActivity(new Intent(getActivity(), HomeActivity.class));
        getActivity().finish();
    }

    private void countTotal(int money){
        int total = SaveSharedPreferences.getTotalOutcome(getActivity());
        int currentUserMoney = SaveSharedPreferences.getCurrentMoney(getActivity());
        total += money;
        currentUserMoney = currentUserMoney <= money ? 0 : (currentUserMoney - money);
        SaveSharedPreferences.setTotalOutcome(getActivity(), total);
        SaveSharedPreferences.setCurrentMoney(getActivity(), currentUserMoney);
    }
}
