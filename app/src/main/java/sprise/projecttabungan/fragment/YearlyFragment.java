package sprise.projecttabungan.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import sprise.projecttabungan.R;
import sprise.projecttabungan.adapter.HistoryMonthlyYearlyAdapter;
import sprise.projecttabungan.adapter.HistoryYearAdapter;
import sprise.projecttabungan.databasesqlite.TransactionDAO;
import sprise.projecttabungan.model.YearTransaction;
import sprise.projecttabungan.util.DateHelper;

/**
 * Created by Andrea's on 9/6/2016.
 */
public class YearlyFragment extends Fragment implements View.OnClickListener{
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TransactionDAO transactionDAO;
    private HistoryYearAdapter adapter;
    private List<YearTransaction> data;

    private RecyclerView yearListRv;
    private TextView pickYearTv;

    private boolean isDateChosen = false;
    private String chosenDate = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        transactionDAO = new TransactionDAO();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_yearly_list, container, false);
        yearListRv = (RecyclerView) view.findViewById(R.id.yearly_rv);
        pickYearTv = (TextView) view.findViewById(R.id.txt_year);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pickYearTv.setOnClickListener(this);
        setRecyclerViewHistory();
        populate();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_year:
                showDatePickerDialog();
                break;
        }
    }

    private void setRecyclerViewHistory(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        yearListRv.setLayoutManager(linearLayoutManager);
        adapter = new HistoryYearAdapter(getActivity());
        yearListRv.setAdapter(adapter);
    }

    private void showDatePickerDialog(){
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(getActivity()
                , new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                chosenDate = dateFormatter.format(newDate.getTime());
                isDateChosen = true;
                populate();
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void populate() {
        if (!isDateChosen)
            chosenDate = new DateHelper().getCurrentDate();
        data = transactionDAO.getYearlyList(getActivity(), chosenDate);
        adapter.setData(data);
        setYearToView(chosenDate);
    }

    private void setYearToView(String chosenDate){
        String[] dates = chosenDate.split("-");
        pickYearTv.setText(dates[0]);
    }
}
