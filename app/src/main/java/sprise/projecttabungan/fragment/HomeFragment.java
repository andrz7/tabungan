package sprise.projecttabungan.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import sprise.projecttabungan.activity.AddMoney;
import sprise.projecttabungan.activity.HomeActivity;
import sprise.projecttabungan.R;
import sprise.projecttabungan.activity.ShowHistory;
import sprise.projecttabungan.databasesqlite.DataHelper;
import sprise.projecttabungan.databasesqlite.TransactionDAO;
import sprise.projecttabungan.util.AppConstant;
import sprise.projecttabungan.util.DateHelper;
import sprise.projecttabungan.util.SaveSharedPreferences;

/**
 * Created by Andrea's on 5/3/2016.
 */
public class HomeFragment extends Fragment implements Button.OnClickListener {

    private static final String TAG = HomeFragment.class.getSimpleName();
    private static final int TOOLBAR_ELEVATION = 0;

    private TransactionDAO transactionDAO;
    private HomeActivity homeActivity;

    private TextView textCurrentMoneyView, incomeTextView, outcomeTextView;
    private Button buttonIncome, buttonOutcome, buttonShowHistory;
    private String willShownCurrentUserMoney, willShownTotalIncome
            , willShownTotalOutcome;

    private long currentUserMoney, totalIncome, totalOutcome;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity = (HomeActivity) getActivity();
        transactionDAO = new TransactionDAO();
        homeActivity.setToolbarTitle(getString(R.string
                .fragment_home_toolbar_title), TOOLBAR_ELEVATION);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        textCurrentMoneyView = (TextView) view.findViewById(R.id.txt_current_money_show);
        outcomeTextView = (TextView) view.findViewById(R.id.outcome_txt_view);
        incomeTextView = (TextView) view.findViewById(R.id.income_txt_view);
        buttonIncome = (Button) view.findViewById(R.id.button_add_income);
        buttonOutcome = (Button) view.findViewById(R.id.button_add_outcome);
        buttonShowHistory = (Button) view.findViewById(R.id.show_history_btn);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buttonOutcome.setOnClickListener(this);
        buttonIncome.setOnClickListener(this);
        buttonShowHistory.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        totalIncome = transactionDAO.getTotalIncome(getActivity(), new DateHelper().getCurrentDate());
        totalOutcome = transactionDAO.getTotalOutcome(getActivity(), new DateHelper().getCurrentDate());

        if(totalOutcome > totalIncome)
            currentUserMoney = 0;
        else
            currentUserMoney = totalIncome - totalOutcome ;

        willShownCurrentUserMoney = String.format("%,d", currentUserMoney);
        willShownTotalIncome = String.format("%,d", totalIncome);
        willShownTotalOutcome = String.format("%,d", totalOutcome);
        textCurrentMoneyView.setText("IDR " + willShownCurrentUserMoney);
        incomeTextView.setText(willShownTotalIncome);
        outcomeTextView.setText(willShownTotalOutcome);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_add_income:
                Log.v(TAG, "masuk income");
                    goToNextPage(AppConstant.CONSTANT_TYPE_ADD_INCOME);
                break;
            case R.id.button_add_outcome:
                Log.v(TAG, "masuk outcome");
                    goToNextPage(AppConstant.CONSTANT_TYPE_ADD_OUTCOME);
                break;
            case R.id.show_history_btn:
                    startActivity(new Intent(getActivity(), ShowHistory.class));
                break;
        }
    }

/*    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_home_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_add_money:
                goToNextPage(AppConstant.CONSTANT_TYPE_ADD_USER_MONEY);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    private void goToNextPage(String typeAddMoney){
        Intent intent = new Intent(getActivity(), AddMoney.class);
        intent.putExtra(AppConstant.EXTRA_TYPE_ADD_MONEY
                , typeAddMoney);
        startActivity(intent);
    }
}
