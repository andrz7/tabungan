package sprise.projecttabungan.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import sprise.projecttabungan.R;
import sprise.projecttabungan.activity.AddMoney;
import sprise.projecttabungan.activity.HomeActivity;
import sprise.projecttabungan.model.WishModel;
import sprise.projecttabungan.util.AppConstant;
import sprise.projecttabungan.util.SaveSharedPreferences;

/**
 * Created by Andrea's on 5/18/2016.
 */
public class WishViewFragment extends Fragment implements View.OnClickListener {

    private final String TAG = WishViewFragment.class.getSimpleName();
    private final int TOOLBAR_ELEVATION = 20;

    private TextView txtViewGoalName, txtViewStartDate, txtViewEndDate
            , txtViewSavedCash, txtViewCompletionCash;
    private ImageView imgViewGoalPic;
    private Button addMoneyButton;
    private WishModel wishObj;

    private String goalName, goalStartDate, goalEndDate
            , goalSavedCash, goalCompletionCash, goalImageLink;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeActivity homeActivity = (HomeActivity) getActivity();
        homeActivity.setToolbarTitle(getString(R.string.fragment_title_wishView), TOOLBAR_ELEVATION);
        setHasOptionsMenu(true);

        wishObj = SaveSharedPreferences.getWishModel(getActivity());
        goalCompletionCash = String.valueOf(wishObj.getGoalPrice());
        goalStartDate = wishObj.getGoalStartDate();
        goalImageLink = wishObj.getGoalImageUrl();
        goalEndDate = wishObj.getGoalEndDate();
        goalName = wishObj.getGoalName();
        goalSavedCash = String.valueOf(SaveSharedPreferences.getSavedWishCash(getActivity()));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.wishlist_fragment_main, container, false);
        txtViewCompletionCash = (TextView) view.findViewById(R.id.txt_view_target_price);
        txtViewStartDate = (TextView) view.findViewById(R.id.txt_view_start_date);
        txtViewSavedCash = (TextView) view.findViewById(R.id.txt_view_saved_cash);
        txtViewGoalName = (TextView) view.findViewById(R.id.txt_view_goal_name);
        txtViewEndDate = (TextView) view.findViewById(R.id.txt_view_end_date);
        imgViewGoalPic = (ImageView) view.findViewById(R.id.img_view_goal);
        addMoneyButton = (Button) view.findViewById(R.id.add_money_btn);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToView();
        addMoneyButton.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        goalSavedCash = String.valueOf(SaveSharedPreferences.getSavedWishCash(getActivity()));
        setToView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.add_money_btn:{
                Intent intent = new Intent(getActivity(), AddMoney.class);
                intent.putExtra(AppConstant.EXTRA_TYPE_ADD_MONEY, AppConstant.CONSTANT_TYPE_WISH_ADD_MONEY);
                SaveSharedPreferences.setFromWish(getActivity(), true);
                startActivity(intent);
            }
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_wishview_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_delete_wish:
                deleteWishAction();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setToView(){
        txtViewGoalName.setText(goalName);
        txtViewStartDate.setText(goalStartDate);
        txtViewEndDate.setText(goalEndDate);
        txtViewSavedCash.setText("IDR. " + goalSavedCash + ".00");
        txtViewCompletionCash.setText("IDR. " + goalCompletionCash + ".00");
        if (goalImageLink != null)
            loadImageFromUrl(goalImageLink);
    }

    private void loadImageFromUrl(String url){
        Glide.with(getActivity()).load(url).into(imgViewGoalPic);
    }

    private void deleteWishAction(){
        new MaterialDialog.Builder(getActivity())
                .title(R.string.dialog_wish_view_title)
                .content(R.string.dialog_wish_view_content)
                .positiveText(R.string.dialog_positive_text)
                .negativeText(R.string.dialog_negative_text)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        setAddFragment();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    }
                })
                .show();
    }

    private void setAddFragment(){
        SaveSharedPreferences.setWishModel(getActivity(), null);
        SaveSharedPreferences.setSavedWishCash(getActivity(), 0);

        final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_view_home, new AddWishlistFragment());
        ft.commit();
    }


}
