package sprise.projecttabungan.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import sprise.projecttabungan.R;
import sprise.projecttabungan.util.SaveSharedPreferences;

/**
 * Created by Andrea's on 5/6/2016.
 */
public class AddMoneyFragment extends Fragment {

    private static final String TAG = AddMoneyFragment.class.getSimpleName();

    private EditText amountMoney;

    private String amountMoneyInStr;
    private int amountMoneyInInt;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_money, container, false);
        amountMoney = (EditText) view.findViewById(R.id.edit_text_income);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEditTextEvent();
    }

    public void setEditTextEvent(){
        amountMoney.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE){
                    amountMoneyInStr = amountMoney.getText().toString();
                    if (amountMoneyInStr.isEmpty())
                        amountMoney.setError("Fill Your Money");
                    else{
                        if (SaveSharedPreferences.getFromWish(getActivity()))
                            addMoneyForWishGoal(amountMoneyInStr);
                        else
                            saveAmountofMoney(amountMoneyInStr);
                    }
                    return true;
                }else{
                    return false;
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SaveSharedPreferences.setFromWish(getActivity(), false);
    }

    private void saveAmountofMoney(String userMoney){
        amountMoneyInInt = Integer.parseInt(userMoney);
        Log.v(TAG,String.valueOf(userMoney));
        SaveSharedPreferences.setCurrentMoney(getActivity(), amountMoneyInInt);
        getActivity().finish();
    }

    private void addMoneyForWishGoal(String userMoney){
        amountMoneyInInt = Integer.parseInt(userMoney);
        int savedMoney = SaveSharedPreferences.getSavedWishCash(getActivity());
        amountMoneyInInt += savedMoney;
        Log.v(TAG,"total uang anda dulu : " + savedMoney +"total uang sekarang : " + String.valueOf(amountMoneyInInt));
        SaveSharedPreferences.setSavedWishCash(getActivity(), amountMoneyInInt);
        getActivity().finish();
    }
}
