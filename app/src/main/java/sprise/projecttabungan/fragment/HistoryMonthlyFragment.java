package sprise.projecttabungan.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import sprise.projecttabungan.R;
import sprise.projecttabungan.adapter.HistoryAdapter;
import sprise.projecttabungan.adapter.HistoryMonthlyYearlyAdapter;
import sprise.projecttabungan.databasesqlite.TransactionDAO;
import sprise.projecttabungan.model.TransactionHIstoryMonthly;
import sprise.projecttabungan.model.TransactionHistoryModel;
import sprise.projecttabungan.util.DateHelper;

/**
 * Created by Andrea's on 6/11/2016.
 */
public class HistoryMonthlyFragment extends Fragment implements View.OnClickListener {
    private RecyclerView monthlyyRecycler;
    private TextView  selectDateTextView;

    private List<TransactionHIstoryMonthly> data;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private HistoryMonthlyYearlyAdapter adapter;
    private TransactionDAO transactionDAO;

    private String chosenDate = "";
    private boolean isDateChosen = false;
    private long total = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = new ArrayList<>();
        transactionDAO = new TransactionDAO();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_monthly, container, false);
        monthlyyRecycler = (RecyclerView) view.findViewById(R.id.monthly_rv);
        selectDateTextView = (TextView) view.findViewById(R.id.txt_month);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectDateTextView.setOnClickListener(this);
        data = transactionDAO.getMonthlyList(getActivity(), new DateHelper().getCurrentDate());
        setRecyclerViewHistory();
        populate();
    }

    private void setRecyclerViewHistory(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        monthlyyRecycler.setLayoutManager(linearLayoutManager);
        adapter = new HistoryMonthlyYearlyAdapter(getActivity());
        monthlyyRecycler.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_month:
                showDatePickerDialog();
                break;
        }
    }

    private void showDatePickerDialog(){
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(getActivity()
                , new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                chosenDate = dateFormatter.format(newDate.getTime());
                isDateChosen = true;
                populate();
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void populate() {
        if (!isDateChosen)
            chosenDate = new DateHelper().getCurrentDate();
        data = transactionDAO.getMonthlyList(getActivity(), chosenDate);
            adapter.setData(data);
        try {
            selectDateTextView.setText(DateHelper.getMonth(chosenDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
