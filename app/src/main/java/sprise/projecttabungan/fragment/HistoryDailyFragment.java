package sprise.projecttabungan.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import sprise.projecttabungan.R;
import sprise.projecttabungan.activity.ShowHistory;
import sprise.projecttabungan.adapter.CustomItemClickListener;
import sprise.projecttabungan.adapter.HistoryAdapter;
import sprise.projecttabungan.databasesqlite.TransactionDAO;
import sprise.projecttabungan.model.TransactionHistoryModel;
import sprise.projecttabungan.util.DateHelper;
import sprise.projecttabungan.util.SaveSharedPreferences;

/**
 * Created by Andrea's on 6/4/2016.
 */
public class HistoryDailyFragment extends Fragment implements View.OnClickListener {
    private final String TAG = ShowHistory.class.getSimpleName();

    private TextView totalTextView, selectDateTextView, noDataTextView;
    private RecyclerView historyRecycler;

    private List<TransactionHistoryModel> historys;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private HistoryAdapter historyAdapter;
    private TransactionDAO transactionDAO;

    private String chosenDate = "";
    private boolean isDateChosen = false;
    private long total = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        historys = new ArrayList<>();
        transactionDAO = new TransactionDAO();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.history_fragment, container, false);
        historyRecycler = (RecyclerView) view.findViewById(R.id.recycler_history);
        selectDateTextView = (TextView) view.findViewById(R.id.txt_view_date);
        noDataTextView = (TextView) view.findViewById(R.id.txt_view_no_data);
        totalTextView = (TextView) view.findViewById(R.id.txt_view_total);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectDateTextView.setOnClickListener(this);
        setRecyclerViewHistory();
        populate();
    }

    private void setRecyclerViewHistory(){
        Log.v(TAG, historys.size() + "");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        historyRecycler.setLayoutManager(linearLayoutManager);

        historyAdapter = new HistoryAdapter(getActivity(), null);
        historyRecycler.setAdapter(historyAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_view_date:
                showDatePickerDialog();
                break;
        }
    }

    private void showDatePickerDialog(){
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(getActivity()
                , new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                chosenDate = dateFormatter.format(newDate.getTime());
                isDateChosen = true;
                populate();
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    private void populate(){
        if (!isDateChosen)
            chosenDate = new DateHelper().getCurrentDate();

        historys = transactionDAO.getHistoryDaily(getActivity(), chosenDate);
        total = new TransactionDAO().getTotalVersionTwo(getActivity(),chosenDate);

        if (historys.size() > 0){
            historyRecycler.setVisibility(View.VISIBLE);
            noDataTextView.setVisibility(View.GONE);
            historyAdapter.setData(historys);
        }else if (historys.size() <= 0){
            noDataTextView.setVisibility(View.VISIBLE);
            historyRecycler.setVisibility(View.GONE);
        }

        try {
            totalTextView.setText(String.format("Total    : IDR %,d", total));
            selectDateTextView.setText("Date    : " + new DateHelper().sqliteDateFormatter(chosenDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.history_activity_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_daily:
                populate();
                break;
            case R.id.menu_monthly:
                Log.v(TAG, chosenDate);
                transactionDAO.getMonthlyList(getActivity(), chosenDate);
                break;
            case R.id.menu_yearly:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
