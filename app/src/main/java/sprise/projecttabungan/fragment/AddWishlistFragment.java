package sprise.projecttabungan.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import sprise.projecttabungan.R;
import sprise.projecttabungan.activity.HomeActivity;
import sprise.projecttabungan.model.WishModel;
import sprise.projecttabungan.util.SaveSharedPreferences;

/**
 * Created by Andrea's on 5/17/2016.
 */
public class AddWishlistFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = AddWishlistFragment.class.getSimpleName();
    private static final int TOOLBAR_ELEVATION = 20;

    private EditText editNameGoal, editGoalPrice, editGoalStartDate, editGoalEndDate;
    private DatePickerDialog startDatePickerDialog, endDatePickerDialog;
    private TextView txtViewShowLink;
    private CircularImageView photoPicImgView;
    private Button submitButton;

    private String goalName, goalImageLink, goalStartDate, goalEndDate, goalPrice;
    private SimpleDateFormat dateFormatter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HomeActivity activity = (HomeActivity) getActivity();
        activity.setToolbarTitle(getString(R.string.title_add_wish), TOOLBAR_ELEVATION);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wishlist_add, container, false);
        photoPicImgView = (CircularImageView) view.findViewById(R.id.image_add_pict);
        editGoalStartDate = (EditText) view.findViewById(R.id.edit_text_start_date);
        editGoalEndDate = (EditText) view.findViewById(R.id.edit_text_end_date);
        txtViewShowLink = (TextView) view.findViewById(R.id.txt_view_show_link);
        editNameGoal = (EditText) view.findViewById(R.id.edit_text_mission);
        editGoalPrice = (EditText) view.findViewById(R.id.edit_text_price);
        submitButton = (Button) view.findViewById(R.id.submit_button);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editGoalStartDate.setOnClickListener(this);
        photoPicImgView.setOnClickListener(this);
        editGoalEndDate.setOnClickListener(this);
        submitButton.setOnClickListener(this);
        editGoalStartDate.setInputType(InputType.TYPE_NULL);
        editGoalEndDate.setInputType(InputType.TYPE_NULL);
        setDatePickerDialog();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.image_add_pict:
                showAddUrlDialog();
                break;
            case R.id.submit_button:
                validateSubmitWish();
                break;
            case R.id.edit_text_start_date:
                startDatePickerDialog.show();
                break;
            case R.id.edit_text_end_date:
                endDatePickerDialog.show();
                break;
        }
    }

    private void setDatePickerDialog(){
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();
        startDatePickerDialog = new DatePickerDialog(getActivity()
                , new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editGoalStartDate.setText(dateFormatter.format(newDate.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));

        endDatePickerDialog= new DatePickerDialog(getActivity()
                , new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editGoalEndDate.setText(dateFormatter.format(newDate.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH));
    }

    private void validateSubmitWish(){
        goalName = editNameGoal.getText().toString();
        goalPrice = editGoalPrice.getText().toString();
        goalStartDate = editGoalStartDate.getText().toString();
        goalEndDate = editGoalEndDate.getText().toString();
        if (goalName.isEmpty())
            editNameGoal.setError(getString(R.string.empty_goalname_edit_text));
        else if (goalPrice.isEmpty())
            editGoalPrice.setError(getString(R.string.empty_goalprice_edit_text));
        else if (goalStartDate.isEmpty())
            editGoalStartDate.setError(getString(R.string.empty_startdate_edit_text));
        else if (goalEndDate.isEmpty())
            editGoalEndDate.setError(getString(R.string.empty_finishdate_edit_text));
        else {
            Log.v(TAG, "save");
            SaveSharedPreferences.setWishModel(
                    getActivity()
                    , new WishModel(goalImageLink, goalName, goalStartDate, goalEndDate
                            , convertToInt(goalPrice))
            );
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_view_home, new WishViewFragment());
            ft.commit();
        }
    }

    private int convertToInt(String price){
        return Integer.parseInt(price);
    }

    private void showAddUrlDialog(){
        new MaterialDialog.Builder(getActivity())
                .title(R.string.fragment_dialog_wish_title)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .inputRange(8, 1000,getResources().getColor(R.color.dark_color_red))
                .input(R.string.fragment_dialog_wish_hint_button
                        , R.string.fragment_dialog_wish_prefill
                        , new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        goalImageLink = String.valueOf(input);
                        txtViewShowLink.setText(goalImageLink);
                    }
                }).show();
    }

}
