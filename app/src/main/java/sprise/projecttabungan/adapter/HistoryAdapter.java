package sprise.projecttabungan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import sprise.projecttabungan.R;
import sprise.projecttabungan.model.TransactionHistoryModel;

/**
 * Created by Andrea's on 6/4/2016.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.Holder> {

    private CustomItemClickListener customItemClickListener;
    private List<TransactionHistoryModel> data;
    private Context context;

    public HistoryAdapter(Context context, CustomItemClickListener customItemClickListener) {
        this.customItemClickListener = customItemClickListener;
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_item, parent, false);
        final Holder holder = new Holder(view);
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                customItemClickListener.onItemClick(v, holder.getAdapterPosition());
//            }
//        });
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (data.get(position).getType().equalsIgnoreCase("income")){
            holder.textViewType.setText("In");
            holder.textViewType.setBackgroundResource(R.drawable.income_circle);
        }else if (data.get(position).getType().equalsIgnoreCase("outcome")){
            holder.textViewType.setText("Out");
            holder.textViewType.setBackgroundResource(R.drawable.outcome_circle);
        }
        holder.textViewAmount.setText(String.format("IDR %,d", data.get(position).getAmount()));
        holder.textViewCategory.setText(data.get(position).getCategory());
        holder.dailyType.setText(data.get(position).getType());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<TransactionHistoryModel> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView textViewCategory;
        TextView textViewAmount;
        TextView textViewType;
        TextView dailyType;

        public Holder(View itemView) {
            super(itemView);
            textViewCategory = (TextView) itemView.findViewById(R.id.daily_category_tv);
            textViewAmount = (TextView) itemView.findViewById(R.id.daily_amount_tv);
            textViewType = (TextView) itemView.findViewById(R.id.txt_view_type);
            dailyType = (TextView) itemView.findViewById(R.id.daily_type_tv);
        }

    }
}
