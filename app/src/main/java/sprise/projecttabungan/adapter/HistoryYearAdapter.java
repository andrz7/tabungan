package sprise.projecttabungan.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import sprise.projecttabungan.R;
import sprise.projecttabungan.model.YearTransaction;
import sprise.projecttabungan.util.DateHelper;

/**
 * Created by Andrea's on 9/6/2016.
 */
public class HistoryYearAdapter extends RecyclerView.Adapter<HistoryYearAdapter.Holder> {
    private List<YearTransaction> data;
    private Context context;

    public HistoryYearAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<YearTransaction> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.yearly_card, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        try {
            holder.monthTv.setText(DateHelper.getMonthNameBasedOnInt(Integer.parseInt(data.get(position).getTime())));
            holder.totalIncomeTv.setText(String.format("IDR %,d", data.get(position).getTotalIncome()));
            holder.totalOutcomeTv.setText(String.format("IDR %,d", data.get(position).getTotalOutcome()));
            holder.totalAmountTv.setText(String.format("IDR %,d", data.get(position).getTotalAmount()));
        }catch (Exception e){

        }
        if (data.get(position).getTotalIncome() < data.get(position).getTotalOutcome()){
            holder.container.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
        } else{
            holder.container.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        private TextView monthTv, totalIncomeTv, totalOutcomeTv, totalAmountTv;
        private LinearLayout container;

        public Holder(View itemView) {
            super(itemView);
            monthTv = (TextView) itemView.findViewById(R.id.yearly_month_name);
            totalIncomeTv = (TextView) itemView.findViewById(R.id.year_income_tv);
            totalOutcomeTv = (TextView) itemView.findViewById(R.id.year_outcome_tv);
            totalAmountTv = (TextView) itemView.findViewById(R.id.year_total_tv);
            container = (LinearLayout) itemView.findViewById(R.id.yearly_status_container);
        }
    }
}
