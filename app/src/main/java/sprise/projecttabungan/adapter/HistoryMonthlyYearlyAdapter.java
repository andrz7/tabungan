package sprise.projecttabungan.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import sprise.projecttabungan.R;
import sprise.projecttabungan.model.TransactionHIstoryMonthly;
import sprise.projecttabungan.util.DateHelper;

/**
 * Created by Andrea's on 6/11/2016.
 */
public class HistoryMonthlyYearlyAdapter  extends RecyclerView.Adapter<HistoryMonthlyYearlyAdapter.Holder>{
    private List<TransactionHIstoryMonthly> data;
    private CustomItemClickListener listener;
    private Context context;

    public HistoryMonthlyYearlyAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<TransactionHIstoryMonthly> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.monthly_card, parent, false);
        final Holder holder = new Holder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        DateHelper dateHelper = new DateHelper();
        String shownDate = null, endDate = null;

        String monthlyIncomeTxt = String.format("IDR %,d", data.get(position).getAmountIncome());
        String monthlyOutcomeTxt = String.format("IDR %,d", data.get(position).getAmountOutcome());
        String totalAmount = String.format("IDR %,d", data.get(position).getAmountMonthly());

        try {
            shownDate = dateHelper.sqliteDateFormatter(data.get(position).getWeekStart());
            endDate = dateHelper.sqliteDateFormatter(data.get(position).getWeekEnd());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.monthIncomeTv.setText(monthlyIncomeTxt);
        holder.monthOutcomeTv.setText(monthlyOutcomeTxt);
        holder.totalTv.setText(totalAmount);
        holder.dateTv.setText("from: " + shownDate+ " until: " + endDate);
        holder.weekCountTv.setText("Week " + String.valueOf(position + 1));
        if (data.get(position).getAmountMonthly() < 0){
            holder.statusTv.setText("Danger");
            holder.statusContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.dark_color_red));
        }else {
            holder.statusTv.setText("Save");
            holder.statusContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        TextView monthIncomeTv, monthOutcomeTv, weekCountTv, statusTv, totalTv, dateTv;
        LinearLayout statusContainer;

        public Holder(View itemView) {
            super(itemView);
            statusContainer = (LinearLayout) itemView.findViewById(R.id.status_container);
            monthOutcomeTv = (TextView) itemView.findViewById(R.id.month_outcome_tv);
            monthIncomeTv = (TextView) itemView.findViewById(R.id.month_income_tv);
            weekCountTv = (TextView) itemView.findViewById(R.id.week_count_tv);
            totalTv = (TextView) itemView.findViewById(R.id.month_total_tv);
            dateTv = (TextView) itemView.findViewById(R.id.date_tv);
            statusTv = (TextView) itemView.findViewById(R.id.status_tv);
        }
    }
}
