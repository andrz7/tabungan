package sprise.projecttabungan.adapter;

import android.view.View;

/**
 * Created by Andrea's on 5/8/2016.
 */
public interface CustomItemClickListener {
    void onItemClick(View v, int position);
}
