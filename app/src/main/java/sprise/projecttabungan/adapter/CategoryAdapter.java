package sprise.projecttabungan.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import sprise.projecttabungan.R;

/**
 * Created by Andrea's on 5/8/2016.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.Holder>  {
    private List<String> dataSet = new ArrayList<>();
    private CustomItemClickListener listener;
    private Context context;

    public CategoryAdapter(Context context, List<String> dataSet
            , CustomItemClickListener listener) {
        this.context = context;
        this.dataSet = dataSet;
        this.listener = listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_card_item, parent, false);
        final Holder holder = new Holder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.textViewCategory.setText(dataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView textViewCategory;
        public Holder(View itemView) {
            super(itemView);
            textViewCategory = (TextView) itemView.findViewById(R.id.txt_view_category);
        }

    }
}
